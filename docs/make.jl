using Documenter, Pumas
using Bioequivalence

ENV["COLUMNS"] = 120
ENV["LINES"] = 30

DocMeta.setdocmeta!(Pumas, :DocTestSetup, :(using Pumas, Bioequivalence); recursive=true)
DocMeta.setdocmeta!(Bioequivalence, :DocTestSetup, :(using Bioequivalence, Random, DataFrames); recursive=true)

makedocs(
  modules=Module[Pumas, Bioequivalence],
  doctest=true,
  clean=true,
  format =Documenter.HTML(edit_link = nothing, assets = [joinpath("assets", "custom.css")]),
  sitename="Pumas",
  authors="PumasAI",
  pages = Any[
    "Home" => "index.md",
    "Tutorials" => Any[
      "tutorials/introduction.md",
    ],
    "Basics" => Any[
      "basics/overview.md",
      "basics/models.md",
      "basics/doses_subjects_populations.md",
      "basics/simulation.md",
      "basics/estimation.md",
      "basics/nca.md",
      "basics/be.md",
      "basics/faq.md",
    ],
    "Model Components" => Any[
      "model_components/domains.md",
      "model_components/dosing_control.md",
      "model_components/dynamical_types.md",
    ],
    "Diagnostics" => Any[
      "analysis/diagnostics.md",
    ],
  ]
  )

# Gitlab deployment extensions for Documenter.
#
# Included from https://github.com/JuliaPlots/AbstractPlotting.jl licensed
# under the MIT "Expat" License.
#
# TODO: upstream this comfig to Documenter itself.

struct Gitlab <: Documenter.DeployConfig
    commit_branch::String
    pull_request_iid::String
    repo_slug::String
    commit_tag::String
    pipeline_source::String
end

function Gitlab()
    commit_branch = get(ENV, "CI_COMMIT_BRANCH", "")
    pull_request_iid = get(ENV, "CI_EXTERNAL_PULL_REQUEST_IID", "")
    repo_slug = get(ENV, "CI_PROJECT_PATH_SLUG", "")
    commit_tag = get(ENV, "CI_COMMIT_TAG", "")
    pipeline_source = get(ENV, "CI_PIPELINE_SOURCE", "")
    Gitlab(commit_branch, pull_request_iid, repo_slug, commit_tag, pipeline_source)
end

function Documenter.deploy_folder(
    cfg::Gitlab;
    repo,
    repo_previews = repo,
    devbranch,
    push_preview,
    devurl,
    branch = "gh-pages",
    branch_previews = branch,
    kwargs...,
)

    marker(x) = x ? "✔" : "✘"

    io = IOBuffer()
    all_ok = true

    println(io, "\nGitlab config:")
    println(io, "  Commit branch: \"", cfg.commit_branch, "\"")
    println(io, "  Pull request IID: \"", cfg.pull_request_iid, "\"")
    println(io, "  Repo slug: \"", cfg.repo_slug, "\"")
    println(io, "  Commit tag: \"", cfg.commit_tag, "\"")
    println(io, "  Pipeline source: \"", cfg.pipeline_source, "\"")

    build_type = if cfg.pull_request_iid != ""
        :preview
    elseif cfg.commit_tag != ""
        :release
    else
        :devbranch
    end

    println(io, "Detected build type: ", build_type)

    if build_type == :release
        tag_nobuild = Documenter.version_tag_strip_build(cfg.commit_tag)
        ## If a tag exist it should be a valid VersionNumber
        tag_ok = tag_nobuild !== nothing

        println(
            io,
            "- $(marker(tag_ok)) ENV[\"CI_COMMIT_TAG\"] contains a valid VersionNumber",
        )
        all_ok &= tag_ok

        is_preview = false
        subfolder = tag_nobuild
        deploy_branch = branch
        deploy_repo = repo

    elseif build_type == :preview
        pr_number = tryparse(Int, cfg.pull_request_iid)
        pr_ok = pr_number !== nothing
        all_ok &= pr_ok
        println(
            io,
            "- $(marker(pr_ok)) ENV[\"CI_EXTERNAL_PULL_REQUEST_IID\"]=\"$(cfg.pull_request_iid)\" is a number",
        )
        btype_ok = push_preview
        all_ok &= btype_ok
        is_preview = true
        println(
            io,
            "- $(marker(btype_ok)) `push_preview` keyword argument to deploydocs is `true`",
        )
        ## deploy to previews/PR
        subfolder = "previews/PR$(something(pr_number, 0))"
        deploy_branch = branch_previews
        deploy_repo = repo_previews
    else
        branch_ok = !isempty(cfg.commit_tag) || cfg.commit_branch == devbranch
        all_ok &= branch_ok
        println(
            io,
            "- $(marker(branch_ok)) ENV[\"CI_COMMIT_BRANCH\"] matches devbranch=\"$(devbranch)\"",
        )
        is_preview = false
        subfolder = devurl
        deploy_branch = branch
        deploy_repo = repo
    end

    key_ok = haskey(ENV, "DOCUMENTER_KEY")
    println(io, "- $(marker(key_ok)) ENV[\"DOCUMENTER_KEY\"] exists")
    all_ok &= key_ok

    print(io, "Deploying to folder \"$(subfolder)\": $(marker(all_ok))")
    @info String(take!(io))

    return Documenter.DeployDecision(;
        all_ok = all_ok,
        branch = deploy_branch,
        repo = deploy_repo,
        subfolder = subfolder,
        is_preview = is_preview,
    )
end

Documenter.authentication_method(::Gitlab) = Documenter.SSH

Documenter.documenter_key(::Gitlab) = ENV["DOCUMENTER_KEY"]

deploydocs(
    repo = "github.com/PumasAI/PumasDocs.jl.git",
    deploy_config = Gitlab(),
    push_preview = true,
    devurl = "dev",
    versions = ["stable" => "v^", "v#.#"],
)
